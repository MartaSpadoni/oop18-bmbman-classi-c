﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;

using System.Windows.Forms;

namespace BmbManCs
{
    public partial class Form1 : Form
    {
        private PictureBox pictureBox1 = new PictureBox();
        private HeroView h = new HeroView(new Point(200, 300));

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.BackColor = Color.White;
            // Connect the Paint event of the PictureBox to the event handler method.
            pictureBox1.Paint += new PaintEventHandler(PictureBox1_Paint);
            pictureBox1.MouseClick += new MouseEventHandler(PictureBox1_Update);
            // Add the PictureBox control to the Form.
            this.Controls.Add(pictureBox1);
        }

        private void PictureBox1_Paint(object sender, PaintEventArgs e)
        {
            h.Render(e.Graphics);
        }
        private void PictureBox1_Update(object sender, EventArgs e)
        {
            pictureBox1.Refresh();
        }
    }
}
