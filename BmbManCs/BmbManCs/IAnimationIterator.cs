﻿
using System.Collections.Generic;
using System.Drawing;

namespace BmbManCs
{
    /// <summary>
    /// interface to model an iterator on Animation
    /// Each element is an Image
    /// </summary>
    public interface IAnimationIterator
    {
        /// <summary>
        /// Return boolean value if the next element is present
        /// </summary>
        /// <returns>true if there is the next frame</returns>
        bool HasNext();
        /// <summary>
        /// Used to next the next frame
        /// </summary>
        /// <returns>the next image in the animation</returns>
        Image GetNextImage();

    }
}