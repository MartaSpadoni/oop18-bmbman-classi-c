﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace BmbManCs
{
    class HeroView : AbstractEntityView
    {
        private const int dim = 48;
        private readonly InfiniteAnimationIterator it;
        /// <summary>
        /// HeroView's Constructors
        /// </summary>
        /// <param name="position"> HeroView's position</param>
        public HeroView(Point position)
            :base(position, new Dimension(dim, dim))
        {
            it = (InfiniteAnimationIterator)Animation.CreateAnimation("heroD//heroD", 3, ".png").CreateInfiniteIterator();
        }
        /// <inheritdoc/>
        public override Image GetSprite()
        {
            return it.GetNextImage();
        }
    }
}
