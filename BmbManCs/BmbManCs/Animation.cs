﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;

namespace BmbManCs
{
    class Animation : IAnimation
    {
        private static readonly int FIRST = 1;
        private List<Image> animation = new List<Image>();

        private Animation() { }
        /// <inheritdoc/>
        public Image GetImageAt(int index)
        {
            return index < animation.Count && index >= 0 ? animation[index] : animation[FIRST];
        }
        /// <inheritdoc/>
        public int Size()
        {
            return animation.Count;
        }
        private void AddFrame(Image img)
        {
            animation.Add(img);
        }
        /// <summary>
        /// Static method to create an Animation
        /// </summary>
        /// <param name="path">first part of frames' path</param>
        /// <param name="frame">number of frame</param>
        /// <param name="type">file extension</param>
        /// <returns></returns>
        public static IAnimation CreateAnimation(string path, int frame, string type)
        {
            Animation an = new Animation();
            for (int i = 0; i < frame; i++) {
                an.AddFrame(Image.FromFile(path + (i+1) + type));
            }
            return an;
        }
        /// <inheritdoc/>
        public IAnimationIterator CreateInfiniteIterator()
        {
            return new InfiniteAnimationIterator(this);
        }

    }
}
