﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BmbManCs
{
    abstract class AbstractEntityView : IEntityView
    {
        ///<inheritdoc/>
        public Point Position { get; set; }
        /// <inheritdoc/>
        public Dimension Dimension { get; set; }
        /// <inheritdoc/>
        public Direction Direction { get; set; }
        /// <inheritdoc/>
        public bool Visible { get; set; }
        /// <summary>
        /// Constructor for AbstarctEntityView
        /// </summary>
        /// <param name="position">EntittyView's position</param>
        /// <param name="dimension">EntityView's dimension</param>
        protected AbstractEntityView(Point position, Dimension dimension)
        {
            Position = position;
            Dimension = dimension;
            Direction = Direction.IDLE;
            Visible = true;
        }
        /// <inheritdoc/>
        public abstract Image GetSprite();
        /// <inheritdoc/>
        public void Render(Graphics g)
        {
            if (Visible)
            {
                g.DrawImage(GetSprite(), Position);
            }
        }
        /// <inheritdoc/>
        public void Remove()
        {
            Visible = false;
        }
    }
}
