﻿using System.Drawing;

namespace BmbManCs
{
    /// <summary>
    ///  Used to model the concept of Animation in our game.
    /// </summary>
    public interface IAnimation
    {
        /// <summary>
        /// How long is the animation, the number of frame in it.
        /// </summary>
        /// <returns>integer value</returns>
        int Size();
        /// <summary>
        /// Used to get a specific frame in the animation
        /// </summary>
        /// <param name="index">frame number</param>
        /// <returns>the frame</returns>
        Image GetImageAt(int index);
        /// <summary>
        /// Used to create an infinite iterator on the animation
        /// </summary>
        /// <returns></returns>
        IAnimationIterator CreateInfiniteIterator(); 
    }
}
