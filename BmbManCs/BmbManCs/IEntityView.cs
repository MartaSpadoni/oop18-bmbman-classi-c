﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace BmbManCs
{
    /// <summary>
    /// Interface to model the general view aspects of each entity in the game.
    /// </summary>
    interface IEntityView
    {
        /// <summary>
        /// EntityView position property.
        /// </summary>
        Point Position {get; set;}
        /// <summary>
        /// EntityView dimension property.
        /// </summary>
        Dimension Dimension { get; set;}
        /// <summary>
        /// EntityView direction property.
        /// </summary>
        Direction Direction { get; set;}
        /// <summary>
        /// EntityView visible property.
        /// </summary>
        bool Visible { get; set; }
        /// <summary>
        /// Method to update entityView image.
        /// </summary>
        /// <param name="g"> the graphics to update</param>
        void Render(Graphics g);
        /// <summary>
        /// Used to remove entityView.
        /// </summary>
        void Remove();
        /// <summary>
        /// Used to get the image associated with the entityView.
        /// </summary>
        /// <returns></returns>
        Image GetSprite();

        
    }
}
