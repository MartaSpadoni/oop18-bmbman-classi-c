﻿using System.Collections.Generic;
using System.Drawing;

namespace BmbManCs
{
    class InfiniteAnimationIterator : IAnimationIterator
    {
        private readonly Animation animation;
        private int frame;
        /// <summary>
        /// Constructor for infiniteAnimationIterator
        /// </summary>
        /// <param name="animation">the animation on which to build the iterator</param>
        public InfiniteAnimationIterator(Animation animation)
        {
            this.animation = animation;
            frame = -1;
        }
        /// <inheritdoc/>
        public Image GetNextImage()
        {
            frame++;
                if (frame >= animation.Size())
                {
                   frame = 0;
                }
                return this.animation.GetImageAt(frame);
        }
        /// <inheritdoc/>
        public bool HasNext()
        {
            return true;
        }

    }
}
