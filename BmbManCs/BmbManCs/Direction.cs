﻿namespace BmbManCs
{
    public class Direction
    {
        /// <summary>
        /// Possible Direction in our game.
        /// </summary>
        public static readonly Direction IDLE = new Direction("idle");
        public static readonly Direction RIGHT = new Direction("left");
        public static readonly Direction LEFT = new Direction("right");
        public static readonly Direction UP = new Direction("up");
        public static readonly Direction DOWN = new Direction("down");
        /// <summary>
        /// Name property.
        /// </summary>
        public string Name { get; private set; }

        private Direction(string name) => Name = name;
        /// <summary>
        /// Static method to get the opposite direction
        /// </summary>
        /// <param name="d">a direction</param>
        /// <returns>the opposite direction of the input</returns>
        public static Direction GetOpposite(Direction d)
        {
            switch (d.Name)
            {
                case "right":
                    return LEFT;
                case "left":
                    return RIGHT;
                case "up":
                    return DOWN;
                case "down":
                    return UP;
                default:
                    return IDLE;
            }
        }
    }
}