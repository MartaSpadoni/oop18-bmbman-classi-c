﻿namespace BmbManCs
{
    /// <summary>
    /// Class to manage the dimension of entities.
    /// </summary>
    public struct Dimension
    {
        /// <summary>
        /// Height property.
        /// </summary>
        public int Height { get;  set; }
        /// <summary>
        /// width property
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// Constructor for Dimension
        /// </summary>
        /// <param name="height">height in pixel</param>
        /// <param name="width">width in pixel</param>
        public Dimension(int height, int width)
        {
            Height = height;
            Width = width;
        }
    }
}